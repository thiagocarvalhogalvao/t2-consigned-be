import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  // username: 'postgres',
  password: 'postgres',
  database: 'postgres',
  username: 'postgres',
  // password: toString(process.env.DATABASE_PASSWORD),
  // database: toString(process.env.DATABASE_NAME),
  entities: ['dist/**/*.entity.{.ts,.js}'],
  synchronize: true,
  autoLoadEntities: true,
  logging: true,
  logger: 'file',
};
