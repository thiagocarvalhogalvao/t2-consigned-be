import { Injectable } from '@nestjs/common';
import { OutputSimulateDto } from './dto/simulate.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { SimulateEntity } from './models/simulate.entity';
import { SimulateI } from './models/simulate.interface';

@Injectable()
export class SimulateService {
  constructor(
    @InjectRepository(SimulateEntity)
    private SimulateRepository: Repository<SimulateEntity>,
  ) { }

  healthCheck() {
    return { status: 'up', environment: process.env['NODE_ENV'] };
  }

  async simulate(simulate: SimulateI): Promise<OutputSimulateDto> {
    try {
      const documentExist = await this.SimulateRepository.findOne({
        where: { document: simulate.document },
      });

      if (!documentExist) {
        await this.SimulateRepository.save(simulate);
      }

      return { amount: 350000, installments: 60 };
    } catch (error) {
      console.log(error.message,);
    }
  }
}
