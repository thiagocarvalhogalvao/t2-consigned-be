import { Test, TestingModule } from '@nestjs/testing';
import { SimulateController } from './simulate.controller';
import { SimulateService } from './simulate.service';

describe('AppController', () => {
  let simulateController: SimulateController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [SimulateController],
      providers: [SimulateService],
    }).compile();

    simulateController = app.get<SimulateController>(SimulateController);
  });

  describe('root', () => {
    it('should return "up!"', () => {
      expect(simulateController.healthCheck()).toBe('up!');
    });
  });
});
