import { Body, Controller, Get, Post } from '@nestjs/common';
import { SimulateService } from './simulate.service';
import { InputSimulateDto, OutputSimulateDto } from './dto/simulate.dto';

@Controller()
export class SimulateController {
  constructor(private readonly simulateService: SimulateService) {}

  @Get('health-check')
  healthCheck() {
    return this.simulateService.healthCheck();
  }

  @Post('simulate')
  async simulate(
    @Body() inputSimulateDto: InputSimulateDto,
  ): Promise<OutputSimulateDto> {
    return await this.simulateService.simulate(inputSimulateDto);
  }
}
