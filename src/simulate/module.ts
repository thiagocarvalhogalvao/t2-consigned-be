import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InputSimulateDto } from './dto/simulate.dto';
import { SimulateEntity } from './models/simulate.entity';
import { SimulateController } from './simulate.controller';
import { SimulateService } from './simulate.service';

@Module({
  imports: [TypeOrmModule.forFeature([SimulateEntity, InputSimulateDto])],
  controllers: [SimulateController],
  providers: [SimulateService],
})
export class SimulateModule {}
