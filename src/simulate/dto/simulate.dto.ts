export class InputSimulateDto {
  document: string;
  cellphone: string;
}

export class OutputSimulateDto {
  amount: number;
  installments: number;
}
